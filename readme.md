st - simple terminal
--------------------
st is a simple terminal emulator for X which sucks less.


Patches
-------

These are patches which applied to my own build of st

* nord color theme
* st-alpha - for transparent windows
* st-bold-is-not-bright
* st-clipboard
* st-scrollback
* st-scrollback-mouse


I've also included a w3m patch in there but didn't bother with it because I
realized I don't really care for images in the terminal that much.

Also st-dracula is in there because I _used_ to use dracula but don't anymore.
It's left there in case I want to go back.


Screenshot
----------

[Screenshot](/ss.png)

Requirements
------------
In order to build st you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (st is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install st (if
necessary as root):

    make clean install


Running st
----------
If you did not install st with make clean install, you must compile
the st terminfo entry with the following command:

    tic -sx st.info

See the man page for additional details.

Credits
-------
Based on Aurélien APTEL <aurelien dot aptel at gmail dot com> bt source code.

